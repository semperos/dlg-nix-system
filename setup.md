# Nix Setup on macOS

## Usage

This section covers ongoing usage of this config, now that it's working. Read below for the odyssey I undertook to get here!

### macOS Upgrades

These can break things, unsurprisingly.

Things I had to do 2020-09 to get my system working properly again:

* Had to move `/etc/shells` out of the way and then run `darwin-rebuild switch`
* Had to add `export PATH="/run/current-system/sw/bin:$PATH"` to the end of my ZShell config.
  * I don't really recall how this was wired up before, to prefer my Nix setup. I thought the Nix paths that get added automatically to my PATH were earlier in the sequence, but I don't recall.
* PATH issues where Nix's things are coming after `/bin` and `/usr/bin`, so:
  * The default `/usr/bin/java` was being invoked by things, resulting in the dreaded macOS popup telling you to install Java 🤦
  * `/bin/bash` which on macOS is an ancient 3.2.57 coming first on PATH rather than Nix-installed 4.4.23
* Had to re-edit my `/etc/pam.d/sudo` file (see below for details)

### nix-prefetch-github

This config uses `fetchFromGithub` which needs details about _what_ to fetch. In particular, I didn't know how to calculate the correct SHA-256 until I found out about [nix-prefetch-github](https://github.com/seppeljordan/nix-prefetch-github).

To upgrade the version of nixpkgs (unstable) I use, I did this:

* Visit [Hydra](https://hydra.nixos.org/jobset/nixos/trunk-combined) to find last good build.
* Click on the build then on the "Inputs" tab.
* Copy the revision.
* At the CLI, replacing the revision with your newer one:
  * `nix-prefetch-github --nix --rev 28fce082c8ca1a8fb3dfac5c938829e51fb314c8 NixOS nixpkgs`
* `darwin-rebuild switch --show-trace`

----

These are the steps I took to get my environment set up using Nix.

----

Everything below this point is current Experiment #1.

## Nix

Installing Nix gives you `nix` and various `nix-*` commands to manage your environment. On macOS Catalina, permissions, mumble mumble, volumes, etc. etc. you have to pass in a special macOS-only flag to the installer, see below.

Also, I overlooked a possibly-important `--daemon` option, but here's what I went with:

1. `curl https://nixos.org/nix/install -O`
1. `chmod +x ./install`
1. `./install --darwin-use-unencrypted-nix-store-volume`

## nix-darwin

On macOS, the [nix-darwin](https://github.com/LnL7/nix-darwin) collection of Nix expressions is critical to getting things set up correctly. It expects to find a `~/.nixpkgs/darwin-configuration.nix` file. When installing/updating your configuration, you make changes to this and rebuild using the commands that nix-darwin makes available.

I found a few helpful resources:

* [awesome-nix](https://nix-community.github.io/awesome-nix/) which I should have looked for first, and which has informational material that is essential reading.
* [This example configuration](https://github.com/LnL7/nix-darwin/blob/master/modules/examples/lnl.nix) from the maintainer of nix-darwin.
* An [example configuration](https://github.com/pSub/configs/blob/master/nix/.config/nixpkgs/config.nix) that binned different sets of tools together, in terms of specifying pkgs.
* [This blog post](https://www.nmattia.com/posts/2018-03-21-nix-reproducible-setup-linux-macos.html#nixos) with the homies repository, which helped solidify how many of the pieces fit together.
* Finally, [kitten/nix-system](https://github.com/kitten/nix-system) which became the basis of the configuration I'm using right now.

In particular, things I'm interested in trying out but put as **out of scope** for this initial configuration:

* Looks like many people use [rycee/home-manager](https://github.com/rycee/home-manager) for managing their dotfiles in Nix code, including the kitten/nix-system that I've based my config on. However, I already have my dotfiles managed nicely in VCS using `stow`, so for now I've commented out the usage of home-manager in the kitten/nix-system code and am going without it for now.
* Many Nix folks use yabai for window tiling and skhd (spelling?) for keyboard shortcut mapping. I'm used to Spectacle, and even though it's no longer actively supported (see below), I'm fine using it for now as I know it works on macOS Catalina.

### Resources for Further Study

I found these after I got things working.

* [Creating a super simple derivation](https://www.sam.today/blog/creating-a-super-simple-derivation-learning-nix-pt-3/)
  * Part of a series of posts on Nix that seem to take things a lot slower (thank you)
* * I found [this post](https://www.softinio.com/post/moving-from-homebrew-to-nix-package-manager/) after I'd figured all this out. Boy, that would have been helpful.
* Dev Setup for Projects: [How to simply get your dependencies with Nix](https://dev.to/monacoremo/how-to-simply-get-your-dependencies-with-nix-2ce1)
  * I've not done this for any _projects_ yet, been focused on system-wide setup.
* [YouTube: Nixpkgs Overlays](https://www.youtube.com/watch?v=W85mF1zWA2o)
  * [Distillation of the Do's and Don't's that Video](https://blog.flyingcircus.io/2017/11/07/nixos-the-dos-and-donts-of-nixpkgs-overlays/)


### Configuration

As mentioned above, nix-darwin expects to find your Nix "entrypoint" at `~/.nixpkgs/darwin-configuration.nix`. So here's what I did:

1. `mkdir -p ~/.nikpgs/`
1. `mkdir -p ~/dev/nix/`
1. `git clone https://github.com/kitten/nix-system.git kitten-system`
1. `ln -s $HOME/dev/nix/kitten-system/configuration.nix $HOME/.nixpkgs/darwin-configuration.nix`

I then modified the kitten/nix-system configuration somewhat extensively, mostly commenting things out. That individual included their GitHub token, GPG key information, preferred fonts, etc., all of which I did not want to use.

Then, **the moment of truth:**

1. `cd ~`
1. `nix-build https://github.com/LnL7/nix-darwin/archive/master.tar.gz -A installer`
1. `./result/bin/darwin-installer`

The last step is the one that validates your Nix code and runs it. You can run it again and again as you tweak your config to get things right.

I then rebooted the computer, because some of the things that printed out during this process frightened me and I wanted to make sure I could boot back into my account:

```
sudo shutdown -r now
```

After making future edits to the configuration, I run this to get a new environment:

```
darwin-rebuild switch
```

I can see the list of generations as well, in case I need to roll back:

```
darwin-rebuild --list-generations
```

NB: This is a _distinct concept_ from `nix-env --list-generations`.

### Manual Steps

Make sure to check out the sub-sections below this list for details about still-not-turnkey aspects of these manual steps.

Also, `. ~/.zshrc` was run many a time to iteratively work through the pieces that were unconfigured at that point.

1. Install XCode.
1. Open XCode and agree to licenses.
1. Run `xcode-select --install` to install CLI tools.
1. Let everything in App Store update.
1. Install DoubleShot in the App Store.
1. Update to latest OS if available.
1. Get Emergency Kits for 1Password accounts (work and personal)
1. Read up on the 1Password CLI `op`, how to log in, how to get items and OTP values.
1. Generate new GPG key with `gpg --generate-key`.
1. Generate new SSH key with `ssh-keygen -t ed25510 -C "daniel@clubhouse"`
1. Download and install Spectacle.
  * Spectacle is no longer actively supported. It links to [Rectangle]() which is an open source alternative based largely on Spectacle (with a compatibility mode to use Spectacle's keyboard shortcuts).
  * The Nix community seems to use yabai for window tiling and skhd (spelling?) for keyboard shortcut management. As written above, this was out of scope for this round.
1. Log into Google on Chrome (daniel@clubhouse.io)
1. Log into Slack app (via Google via Chrome)
1. Log into GitHub on Chrome.
1. Add SSH key using `pbcopy < ~/.ssh/id_ed25510.pub` to copy it and pasting it into GitHub's UI.
1. Add GPG key using `gpg --armor --export 47376F603EFF88AFA2250480055C37C9F56EBECB > ~/gpg-pub-key` and then `pbcopy < ~/gpg-pub-key` to copy it and pasting it into GitHub's UI.
1. Log into GitLab and do the same.
1. Download and install Firefox Developer Edition.
  * Install 1Password extensions for personal 1Password account.
1. Install 1Password Chrome extension (navigate to it from _1Password's_ site) for work 1Password account.
  * I discover it's now _two clicks_ to get to the 1Password extension in Chrome. I get it, if you have a lot of extensions, but 🤦
1. Clone personal config repo to `~/config`
  * Here is where things get interesting, because each folder requires a little something different, especially depending on order (e.g., is there already a `~/.zshrc` in the way). _Going to document these below separately._
1. Install kitty via their one-liner:
  * `curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin`
  * NB: This wasn't necessary. I just didn't know where nix was installing apps. It's under `~/Applications/Nix Apps/`.
1. Clone the clubhouse/backend repo and follow online setup instructions.
1. Install Oh My Zsh and stow my config (see below).
  * Of particular note for setup: this gives me access to shell functions that I use to setup signing per-repo, so that I don't accidentally commit using my personal identity or work identity in the wrong scenario.
  * Have to set FZF_BASE to make oh-my-zsh plugin for fzf happy. Assuming this is `dirname $(which fzf)`.
    * Nope 🤦 Had to do this:
      1. `git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf`
      1. `export FZF_BASE="$HOME/.fzf"`
1. Install NVM using their one-liner:
  * `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash`
  * This does not install a version of node. We'll use clubhouse/node-app to do that.
1. `mkdir ~/clubhouse`
  * If you don't do this, it's a weird symlink when `stow` gets run.
1. `stow clubhouse` (needed by zshell config)
  * As part of that, going to add proper GPG key to zshell config for this laptop.
1. Start cloning Clubhouse repos into `~/clubhouse`
  * Doing this so I can check what version of Node is set in the clubhouse/node-app repo.
  * Also _by memory_ typed this bad boy, since I grew tired of re-entering my SSH key passphrase:
    * `ssh-add -K ~/.ssh/id_ed25519`
1. `cd ~/clubhouse/node-app`
  * With nvm already installed, this automatically installs the correct Node.js version because of the local `.nvmrc` file. w00t!
1. Rust complaining about no default toolchain (had to do a web search on the error message):
  * `rustup install stable`
  * `rustup default stable`
1. Getting closer. Now our script can't find `cred` which is helper I use to keep credentials in encrypted YAML files (using sops).
  * `stow bin`
1. At this point, `. ~/.zshrc` doesn't produce any output. Yay!
1. First meeting, had to install Zoom.
  * It auto-downloaded its own `.pkg` file and I installed it. Done.
1. VPN isn't setup. That's bad.
  * I had `wireguard` in my Nix packages, but it's not _running_ and I'm not going to dig any deeper for now.
  * Going to "just download it". This is definitely per-company, so 🤷
    * Available on App Store.
1. Emacs isn't setup yet. Hello!
1. `mv ~/.emacs.d ~/.emacs.d.orig`
1. Prelude Emacs:
  * NB Don't follow these steps, these are the suggested but they don't work in my setup with stow...read on below.
  * Also, I wrote this and then did a bunch of the stuff after it, because I'm always intimidated by the amount of things I have to retweak every time I land my Emacs...really need it to be entirely static and reproducible, if possible.
  * `mkdir -p ~/dev/emacs/`
  * `git clone git://github.com/bbatsov/prelude.git $HOME/dev/emacs/prelude`
  * `ln -s $HOME/dev/emacs/prelude $HOME/.emacs.d`
  * TODO I want to check out pinned packages in Prelude. Found `prelude-pinned-packages.el` [in the docs](https://prelude.emacsredux.com/en/latest/installation/) this time around.
  * The above are the suggested instructions. I always forget, stow barfs when there's an existing .emacs.d present.
    1. Download ZIP from bbatsov/prelude on GitHub.
    1. Unzip it and place at `~/.emacs.d`
    1. `rm -rf ~/.emacs.d/personal`
    1. `ln -s $HOME/config/emacs-prelude/personal $HOME/.emacs.d/personal`
  * I forgot to pull down my org-mode knowledge base (which I've stopped maintaining in favor of Zettlr with ZK integration). Going to comment out that part of `user.el`.
1. Stow Leiningen
1. Stow Clojure
  * `mv ~/.clojure/deps.edn ~/.clojure/deps.edn.orig`
  * `stow clojure`
1. Install [babashka](https://github.com/borkdude/babashka)
  * `curl -s https://raw.githubusercontent.com/borkdude/babashka/master/install -o install-babashka`
  * `chmod +x install-babashka && ./install-babashka`
  * To test:
    * `ls | bb -i '(filter #(-> % io/file .isDirectory) *input*)'`
1. Getting ready to commit my changes to the kitten/nix-system "clone" (I'm starting with fresh Git history) and I realize I don't have my current personal GPG key on this machine.
  * On personal machine:
    * `gpg --export-secret-keys --armor ID_HERE > ~/daniel.asc`
    * Put that on a "trusted" USB drive 🙄
  * On work machine:
    * Copied file to MBP
    * `gpg --import ~/Documents/daniel.asc`
    * `gpg --list-keys` to grab the key id
    * `gpg --edit-key ID_HERE`
      * Entered `?` and thankfully that showed a bunch of help.
      * `trust`
      * Shows options for different trust levels.
      * `5` which is `ultimate` because this is my key.
  * Ran `use-personal-signingkey` and it quit my terminal because I put an `exit 1` in that bad boy. Removing that.
1. Now a bunch of manual steps from Clubhouse's own clubhouse/backend install.md document.
1. Ended up using Docker for Mac from official website. Docker _client_ installed fine via nixpkgs, but no apparent way to get the proper daemon services started.
1. Downloading GraalVM Community from GitHub (Java 8) to get `native-image` as I continue through Clubhouse instructions.
  * Going to put it _last_ on my path, so I just have `gu` and not `java` from the GraalVM distribution.
  * Did that, then:
    * `gu install native-image`
1. Last piece is getting a Datomic backup local, last one fairly old and script wasn't up-to-date.
1. Downloaded Hammerspoon as an app.
1. Installed `fennel` via `luarocks` (fennel not directly available on nixpkgs)
1. Configured SpaceHammer:
    * `mv ~/.hammerspoon ~/.hammerspoon.orig`
    * `git clone https://github.com/agzam/spacehammer ~/.hammerspoon`
1. Yarn I will install using clubhouse/node-app's instructions, since it requires a specific version.
1. **ATTENTION** You can use Touch ID with sudo by adding `auth    sufficient  pam_tid.so` to the top of `/etc/pam.d/sudo`.
  * This is likely easily done in the Nix config for Darwin.

#### Z Shell with Oh My Zsh

1. Visit [ohmyz.sh](https://ohmyz.sh/#install)
1. Run its one-liner:
  * `sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`
1. `mv ~/.zshrc ~/.zshrc.orig`
1. `cd ~/config`
1. `stow zshell`
1. For this round, had to add configuration for fzf (see Gotchas below).
1. By design, my zshell config tells me it can't find `~/.zsh_daniel` which I use to set per-computer values. Doing that now.
1. Setting `DANIEL_ENV` to `clubhouse` there, I now get a warning (by design) that my Clubhouse-specific zshell script is not found. Time to `stow clubhouse`.

## Gotchas

* fzf installs correctly, but in order to use it as an oh-my-zsh plugin, you have to clone it some place and set `FZF_BASE` so oh-my-zsh knows where to find it.
* NVM is not available in the main Nix channels. Install it manually.
* TexLive, unsurprisingly, took the lion's share of the time to install. I likely should have excluded it for initial build, but I always use it on every computer and I like to get it out of the way early since it's a huge amount to download and install.
* Many packages are Linux-only, but thankfully Nix tells you that and I just commented out apps (all of them were GUI apps) and will install them separately for now. See [this blog post](https://cmacr.ae/post/2020-05-09-managing-firefox-on-macos-with-nix/) for an example of how to write your own custom Nix derivation that pulls down a DMG and does the dirty work for you (that blog post outlines Firefox, but should work for different kinds of apps).
* Packages that begin with a numeral must be preceded by an underscore `_` in Nix code (e.g., `pkgs._1password`)
* Installing Slack worked initially, but when running `darwin-rebuild switch` it complained about Slack not being compatible with Darwin. I removed it from my configuration, but that did not remove it from `/Applications`, even after running `nix-garbage-collect`.
* Alacritty installs as an executable rather than a full-fledged app. So no icon. I may take it out of my Nix configuration and "just install it" to have the (really nice) icon.
  * NB: Nope, this was wrong. As with kitty, I just didn't know where nix was installing *.apps. They're under `~/Applications/Nix Apps/`.
* GraalVM I like to have installed but do not want on my system PATH. So this I will download and put into `~/opt` and then add it to the _end_ of my PATH so that I have access to the `gu` and `native-image` commands.

----

Everything below this point was Experiment #0.

## Preamble

Follow the Catalina-specific instructions for Nix. There's a special flag to pass the installer. Also had to download the installer directly rather than using the all-in-one bash + curl invocation they advertised.

```
mkdir ~/.config/nixpkgs
```

Then make a file at `~/.config/nixpkgs/config.nix` and add these contents:

```nix
{ allowUnfree = true; }
```

## Packages

### 1Password

This installs the `op` 1Password CLI utility.

[1Password Documentation](https://support.1password.com/command-line/)

```
nix-env -i 1password-1.1.1
```
