{ pkgs, ... }:

pkgs.stdenv.mkDerivation rec {
  pname = "Zettlr";
  version = "1.7.2";

  buildInputs = [ pkgs.undmg ];
  sourceRoot = ".";
  phases = [ "unpackPhase" "installPhase"];
  installPhase = ''
mkdir -p "$out/Applications"
cp -r Zettlr.app "$out/Applications/Zettlr.app"
    '';

  src = pkgs.fetchurl {
    name = "Zettlr-${version}.dmg";
    url = "https://github.com/Zettlr/Zettlr/releases/download/v${version}/Zettlr-${version}.dmg";
    sha256 = "f599751beb7d9ec8b7dd36e6326510c6dc97f29b457ffd975efe533b634b1ce0";
  };

  meta = with pkgs.stdenv.lib; {
    description = "Zettlr Markdown Editor";
    homepage = "https://www.zettlr.com/";
    # maintainers = [ maintainers.semperos ];
    platforms = platforms.darwin;
  };

}
