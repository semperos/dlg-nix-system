{
  imports = [
    ./nix/nix-path.nix
    ./modules
    ./profiles/common.nix
  ];
}
