{
  allowUnfree = true;
  packageOverrides = pkgs: {
    leiningen = pkgs.leiningen.override {
      jdk = pkgs.jdk11;
    };
  };
}
