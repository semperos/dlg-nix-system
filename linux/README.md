# Nix on Linux

This folder is dedicated to Nix code that relies on [home-manager](https://github.com/rycee/home-manager) to configure packages for installation.

## Installation

1. Install Nix.
1. Install home-manager.
1. `ln -s /home/daniel/dev/nix/dlg-nix-system/linux/home.nix /home/daniel/.config/nixpkgs/home.nix`
1. `ln -s /home/daniel/dev/nix/dlg-nix-system/linux/config.nix /home/daniel/.config/nixpkgs/config.nix`
1. `home-manager switch`

Repeat the last step—fixing what it tells you is wrong—until it's happy.

## Add, Remove, or Update Packages

1. Change Nix code here. The root is `home.nix`
1. `home-manager switch`

See GitHub or run `man home-configuration.nix` for more details.

