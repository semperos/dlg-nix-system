{ config, pkgs, ... }:

# This configuration is designed for use with home-manager
# under non-NixOS Linux.
{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "daniel";
  home.homeDirectory = "/home/daniel";

  # Lorri: https://github.com/target/lorri
  services.lorri.enable = true;

  imports = [
    # ./alacritty.nix
    ./bat.nix
    ./direnv.nix
    ./emacs.nix
    ./fzf.nix
    ./git.nix
    ./gpg.nix
    ./htop.nix
    ./jq.nix
    # ./tex.nix
    ./tmux.nix
    # ./xserver.nix
    # ./zsh.nix
  ];

  home.packages = [
    pkgs.cloc
    pkgs.clojure
    pkgs.cmark
    pkgs.curl
    pkgs.fd
    pkgs.figlet
    pkgs.glibc
    pkgs.graphviz
    pkgs.nix-prefetch-github
    pkgs.imagemagick
    pkgs.jdk11
    pkgs.leiningen
    pkgs.lilypond
    pkgs.luajit
    pkgs.maven
    pkgs.neovim
    pkgs.niv
    pkgs.p7zip
    pkgs.pdftk
    pkgs.python38
    pkgs.python38Packages.jupyterlab
    pkgs.python38Packages.notebook
    pkgs.python38Packages.pip
    pkgs.python38Packages.setuptools
    pkgs.ripgrep
    pkgs.rlwrap
    # pkgs.rustup
    pkgs.sass
    pkgs.shellcheck
    pkgs.silver-searcher
    pkgs.slack
    pkgs.sops
    pkgs.stow
    # pkgs.texlive.combined.scheme-full
    pkgs.tree
    pkgs.unzip
    pkgs.wget
    pkgs.which
    pkgs.zip
    pkgs.zsh
    # Why not tar and everything else?
    pkgs.gnome3.gnome-tweaks
  ];


  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "20.09";
}
