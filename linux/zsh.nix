{
  programs.zsh = {
    enable = true;
    history = {
      expireDuplicatesFirst = true;
    };
    initExtra = ''
    export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
    '';
    oh-my-zsh = {
      enable = true;
      plugins = [
        "fzf"
        "git"
        "lein"
        "ruby"
        "terraform"
      ];
      theme = "bira";
    };
    sessionVariables = {
      PATH = "$PATH:$HOME/opt/bin:$HOME/.local/bin";
    };
    shellAliases = {
      ll = "ls -l";
      ".." = "cd ..";
    };
  };
}
