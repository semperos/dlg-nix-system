# Nix Configuration

This repository, based on [kitten/nix-system](https://github.com/kitten/nix-system), contains Nix code for use on macOS and Linux that leverages Nix as a package manager.

It might one day be a single config that works seemlessly on both, but for now they are separate and messy.

* `assets` This folder is shared by both macOS and Linux. Sensitive files are encrypted using openssl, which you can see from the Nix code used to decrypt them.
* `linux` This folder's `home.nix` is the dedicated entry point for the Linux configuration. Read its README for Linux-specific instructions.
* `nix` This folder is shared by both configs and handles pinning the URLs and versions that Nix uses for package management.
* Everything else was put into place to get a macOS configuration working using [nix-darwin](https://github.com/LnL7/nix-darwin) as well as home-manager. Not all files are used, so look at which imports are actually used.

## macOS

Read the [setup.md](./setup.md) file for a messy introduction to what I did to learn Nix and get my system set up.

## Current blocker for unifying these configurations?

When I symlinked the main `configuration.nix` file of this repository on Linux with nix installed and tried to run `home-manager switch`, it complained about not knowing what `crossSystem` was about.
