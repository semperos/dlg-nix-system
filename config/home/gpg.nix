let
  inherit (import ../../nix/secrets.nix) readSecretFile;
in {
  home.file.".gnupg/pubring.kbx".source =
    readSecretFile ../../assets/pubring.kbx;
  home.file.".gnupg/private-keys-v1.d/21CF9F2BD5B0E6E272D932BC10938D51F6A7C624.key".source =
    readSecretFile ../../assets/21CF9F2BD5B0E6E272D932BC10938D51F6A7C624.key;
  home.file.".gnupg/private-keys-v1.d/22234A6D345C268494DED477C28B5C93F13ACF8C.key".source =
    readSecretFile ../../assets/22234A6D345C268494DED477C28B5C93F13ACF8C.key;
  home.file.".gnupg/private-keys-v1.d/DEEBE6FC758D0A56BAB5D33F5C9432E6651DE485.key".source =
    readSecretFile ../../assets/DEEBE6FC758D0A56BAB5D33F5C9432E6651DE485.key;
  home.file.".gnupg/private-keys-v1.d/F408F3B1821EA9A52795D7414F904F1B48E99768.key".source =
    readSecretFile ../../assets/F408F3B1821EA9A52795D7414F904F1B48E99768.key;
}
