{
  programs.git = {
    enable = true;
    userName = "Daniel Gregoire";
    userEmail = "daniel@clubhouse.io";

   signing = {
     signByDefault = true;
     # I use different GPG keys for personal and work projects, since
     # I commit with different email addresses. I have a
     # use-personal-signingkey and a use-<name-of-work>-signingkey
     # ZShell functions that I run when cloning a new project that
     # sets both the correct GPG key and the correct email.
     key = "RUN_SCRIPT_TO_SET_PER_PROJECT_KEY";
   };

    ignores = [
      ".DS_Store"
      "*.sw[nop]"
      "*.undodir"
      ".env"
      "*.orig"
      "*.elc"
      "*.class"
      ".dir-locals.el"
      ".nrepl-history"
      ".envrc*"
    ];

   lfs = {
     enable = true;
   };

    aliases = {
      br = "branch";
      co = "checkout";
      glog = "log --oneline --decorate --all --graph";
      last = "log -1";
      lol = "log --oneline";
      prev = "checkout HEAD^";
      pushf = "push --force-with-lease";
      recommit = "commit -a --amend --no-edit";
      st = "status -s";
      up = "pull --rebase --prune";
    };

    delta = {
      enable = true;
      options = [
        "--tabs=2"
        "--theme=base16"
      ];
    };

    extraConfig = {
      color.ui = "auto";
      diff.compactionHeuristic = true;
      diff.submodule = "log";
      fetch.prune = true;
      github.user = "semperos";
      merge.ff = "only";
      pull.rebase = true;
      push.default = "simple";
      status.showUntrackedFiles = "all";
    };
  };
}
