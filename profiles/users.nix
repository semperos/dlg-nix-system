{ pkgs, lib, ... }:

let
  inherit (lib) optionalAttrs mkMerge mkIf mkDefault;
  inherit (lib.systems.elaborate { system = builtins.currentSystem; }) isLinux isDarwin;

  home = import ../config/home/default.nix;
in

mkMerge [
  {
    home-manager.users.dlg = home;
    users.users.dlg.home = mkIf isDarwin "/Users/dlg";
  }

  (optionalAttrs isLinux {
    users.users.dlg = {
      isNormalUser = true;
      uid = 1000;
      home = "/home/dlg";
    };
  })
]
