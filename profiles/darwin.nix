{ pkgs, ... }:

let

in
{
#  imports = [
#    ../config/yabai.nix
#    ../config/skhd.nix
  #  ];

  # imports = [
  #   ../modules/zettlr.nix
  # ];

  system.defaults.dock = {
    autohide = true;
    static-only = true;
  };

  system.keyboard = {
    enableKeyMapping = true;
    remapCapsLockToControl = true;
  };

  environment.systemPackages = [
    pkgs.amazon-ecr-credential-helper
    pkgs.coreutils-full
    pkgs.undmg
    pkgs.jetbrains.idea-community
    # Couldn't get Zettlr to work.
    # Had to define it directly in the `let` above because I don't know Nix well enough yet.
    # When "installed", it had issues with Electron Framework, I don't know...
    # zettlr
#    pkgs.wireguard
#    pkgs.darwin-zsh-completions
#    pkgs.firefox
#    pkgs.jetbrains.idea-community
#    pkgs.slack
#    pkgs.spectacle
#    pkgs.vlc
  ];
}
