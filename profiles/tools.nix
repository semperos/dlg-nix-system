{ pkgs, ... }:

# See config/home/default.nix and sibling files to understand what
# packages are installed by being enabled via home-manager.
{
  environment.systemPackages = with pkgs; [
    alacritty
    asciinema
    aws-vault
    awslogs
    bash
    bat
    # TODO Config babashka (bb)
    cloc
    clojure
    chez
    cmake
    cmark
    curl
    direnv
    emacsMacport
    fd
    figlet
    gdbm
    gforth
    gmp
    guile
    graphviz
    htop
    httpie
    imagemagick
    lout
    lua5_3
    lua53Packages.luarocks
    neofetch
    niv
    nix-prefetch-github
    opam
    openjdk8
    openssl
    pcre
    polyml
    python38
    # python38Packages.jupyterlab
    # python38Packages.notebook
    python38Packages.pip
    jq
    leiningen
    libxml2
    maven
    p7zip
    pkg-config
    postgresql_12
    # racket # Marked as broken for now.
    ripgrep
    rlwrap
    rustup
    sass
    sbcl
    sd
    shellcheck
    silver-searcher
    sops
    stow
    telnet
    tree
    tmux
    unzip
    wget
    which
    zip
  ];
}
